package = "mpm"
version = "0.01-1"
source = {
	url = "",
}

description = {
	summary = "An advanced package manager for use with Minerva Linux",
	homepage = "https://",
	license = "BSD 2-Clause",
}

dependencies = {
	"lua-toml == 2.0",
	"luafilesystem == 1.8.0",
	"lua_cliargs == 3.0",
}


build = {
	type = "builtin",
	modules = {
		main = "src/main.lua",
		a = "src/pkg.lua",
		b = "src/parse.lua",
		c = "src/toml.lua",
	}
}

