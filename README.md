# A WIP Package manager

## How to run
Install LuaJIT (or MoonJIT)

Simply execute the "main.lua" binary in "src"

`./src/main.lua`

alternatively do

`luajit src/main.lua`

It is recommended to use LuaJIT/MoonJIT to use this application as JIT compilation is significantly faster than interpreting. If you would like to use the Lua interpreter do the following

`lua src/main.lua`

In the future, tools will be used to statically link this program with LuaJIT into single binary which can be executed. 

Luastatic can be used to build a single binary. The shell script is in build/build.sh, however this is meant for my personal machine, so it may be finnicky.

## Contributing

Development is done on https://gitea.com, the repository is located at: https://gitea.com/mrva/mpm
