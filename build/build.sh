#!/bin/sh

echo "This is for my personal machine."
mkdir build/target;
pushd build/target
	curl -LO https://github.com/keplerproject/luafilesystem/archive/refs/tags/v1_8_0.tar.gz
	tar xf v1_8_0.tar.gz
popd

pushd build/target/luafilesystem-1_8_0
	make
	ar rcs lfs.a src/lfs.o
	mkdir ../mpm;
	cp lfs.a ../mpm
popd

pushd build/target
	curl -LO https://github.com/amireh/lua_cliargs/archive/refs/tags/v3.0-2.tar.gz
	tar xf v3.0-2.tar.gz
popd


luastatic src/main.lua src/pkg.lua src/parse.lua src/toml.lua src/cliargs.lua /usr/local/lib/libluajit-5.1.a ./build/target/mpm/lfs.a -I/usr/local/include/moonjit-2.2 -o build/target/mpm/mpm

rm main.luastatic.c

echo "If all worked out, the mpm binary should be in ./build/target/mpm/"
