-- Free software released under the terms of the Apache 2.0 License
-- Copyright (c) Ergpopler <theodorekaczynski@ctemplar.com>
toml = require "src.toml"

function parseFileToTable(path)
	local file = io.open(path, "rb");
	if not file then return nil end ;
	local content = file:read "*a" -- *a means read all of the file
	file:close()
	return toml.parse(content);
end

function parseFileToPkgName(path)
	local content = parseFileToTable(path)
	local fullname = content.package.name .. "-" .. content.package.version
	return fullname
end


return {parseFileToTable = parseFileToTable, parseFileToPkgName = parseFileToPkgName}
