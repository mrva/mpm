#!/usr/bin/env luajit
-- Free software released under the terms of the Apache 2.0 License
-- Copyright (c) Ergpopler <theodorekaczynski@ctemplar.com>
local _version = "0.01"
local _release = "Alpha"
local _copyright = "Copyright (c) 2021 'Ergpopler' <theodorekaczynski@ctemplar.com>"
local _license = "BSD 2-Clause"
local _summary = "An advanced package manager"
local _help = [[
MPM (Minerva Package Manager)

Options:
	build {..} -- Builds an MPM package from MPM specfile
	install {..} -- Installs MPM package onto the system
Arguments:
	--help, -h: Show usage information
	--version, -v: Show version information
	--verbose, -V: Be verbose; print more information.

Examples: 
	mpm install ./Package-1.0-1.mpm -V
]]
local function print_version()
  print("MPM (Minerva Package Manager): " .. _version);
  print("Release: " .. _release);
  print("Copyright: " .. _copyright);
  print("License: " .. _license);
  print("Summary: " .. _summary);
  os.exit(0);
end

local function range(from, to, step)
  step = step or 1
  return function(_, lastval)
    local nextval = lastval + step
    if step > 0 and nextval <= to or step < 0 and nextval >= to or
       step == 0
    then
      return nextval
    end
  end, nil, from - step
end


local pkg = require "src.pkg"
local cli = require "src.cliargs"

cli:set_name("mpm")
cli:option("-b TOML", "Builds mpm archive from mpm toml", {})
cli:option("-i PACKAGES...", "Builds mpm archive from mpm toml", {})
cli:flag("-v, --version", "prints the programs version", print_version)
cli:splat("nothing", "placeholder", nil, 10)
local args, err = cli:parse()

if not args and err then
	print(err)
	os.exit(1)
end

if #args.i > 0 then
	print("Things to install:")

	for i, thing in ipairs(args.i) do
		print(" " .. i .. ". " .. thing)
	end
else 
	print("No packages to install were specified.")
end

