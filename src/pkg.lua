-- Free software released under the terms of the Apache 2.0 License
-- Copyright (c) Ergpopler <theodorekaczynski@ctemplar.com>
local parse = require "src.parse"
local lfs = require("lfs")

local builddir = "/var/mpm/source/"
function downloadFromToml(tomlPath)
	local sourceURL = parse.parseFileToTable(tomlPath).package.source
	os.execute("curl -L " .. sourceURL .. "-o " .. builddir .. parse.parseFileToPkgName(tomlPath) .. ".src") 
end

function build(tomlPath) 
	print(lfs.currentdir())
end


return {downloadFromToml = downloadFromToml, build = build}
