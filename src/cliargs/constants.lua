-- Copyright (c) 2012-2015 Ahmad Amireh
return {
  TYPE_COMMAND  = 'command',
  TYPE_ARGUMENT = 'argument',
  TYPE_SPLAT    = 'splat',
  TYPE_OPTION   = 'option',
}
